var mymap = L.map('mapid').setView([4.44812,-75.24000], 27);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiY3JhenlzZWJhcyIsImEiOiJja2RkdnNqdnk0aXI3MzZxcmZiZXQ2YjJvIn0.QEu_oNPviNU6H-8QqD7X9A'
}).addTo(mymap);

var marker = L.marker([4.44812,-75.24000]).addTo(mymap);
